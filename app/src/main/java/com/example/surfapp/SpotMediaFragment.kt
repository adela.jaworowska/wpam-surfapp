package com.example.surfapp

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.activityViewModels
import com.example.surfapp.databinding.FragmentSpotMediaBinding
import com.example.surfapp.databinding.FragmentWeatherDetailsBinding

/**
 * A simple [Fragment] subclass.
 * Use the [SpotMediaFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class SpotMediaFragment : Fragment() {

    private val viewModel: TitleViewModel by activityViewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        //return inflater.inflate(R.layout.fragment_spot_media, container, false)

        val binding = DataBindingUtil.inflate<FragmentSpotMediaBinding>(inflater, R.layout.fragment_spot_media, container, false)

        // Giving the binding access to the OverviewViewModel
        binding.viewModel = viewModel

        binding.lifecycleOwner = this


        binding.spotMediaLayout.setBackgroundResource(R.drawable.top_view_sand_meeting_seawater)

        return binding.root
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment SpotMediaFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            SpotMediaFragment().apply {
            }
    }
}