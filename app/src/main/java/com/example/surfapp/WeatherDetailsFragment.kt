package com.example.surfapp

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.ViewModelProvider
import com.example.surfapp.databinding.FragmentWeatherDetailsBinding


/**
 * A simple [Fragment] subclass.
 * Use the [WeatherDetailsFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class WeatherDetailsFragment : Fragment() {

    /**
     * Lazily initialize our [TitleViewModel].
     */
//    private lateinit var viewModel: TitleViewModel
//    // Use the 'by activityViewModels()' Kotlin property delegate
//    // from the fragment-ktx artifact
    private val viewModel: TitleViewModel by activityViewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        //return inflater.inflate(R.layout.fragment_weather_details, container, false)

//        viewModel = ViewModelProvider(this).get(TitleViewModel::class.java)
        val binding = DataBindingUtil.inflate<FragmentWeatherDetailsBinding>(inflater, R.layout.fragment_weather_details, container, false)

        // Giving the binding access to the OverviewViewModel
        binding.viewModel = viewModel

        binding.lifecycleOwner = this

        binding.weatherDetailsLayout.setBackgroundResource(R.drawable.top_view_sand_meeting_seawater)

        //(activity as AppCompatActivity?)!!.supportActionBar!!.setTitle(viewModel.spotName.value) //=viewModel.spotName.value

        return binding.root
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment WeatherDetailsFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            WeatherDetailsFragment().apply {

            }
    }
}