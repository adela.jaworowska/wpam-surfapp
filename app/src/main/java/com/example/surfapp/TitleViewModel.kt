package com.example.surfapp

import Event
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.surfapp.network.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class TitleViewModel : ViewModel() {

    // The internal MutableLiveData String that stores the status of the most recent request
    private val _failure = MutableLiveData<Boolean>()

    // The external immutable LiveData for the request status String
    val failure: LiveData<Boolean>
        get() = _failure

    private val _success = MutableLiveData<Boolean>()

    val success: LiveData<Boolean>
        get() = _success

    private val _spotsFetched = MutableLiveData<Boolean>()

    val spotsFetched: LiveData<Boolean>
        get() = _spotsFetched

    private val _weatherCondition = MutableLiveData<Hourly>()

    val weatherCondition: LiveData<Hourly>
        get() = _weatherCondition

    private val _waveHeight = MutableLiveData<String>()

    val waveHeight: String?
        get() = _weatherCondition.value?.waveHeight

    private val _spots = MutableLiveData<List<SurfSpot>>()

    val spots: LiveData<List<SurfSpot>>
        get() = _spots

    private val _spotName = MutableLiveData<String>()

    val spotName: LiveData<String>
        get() = _spotName

    private val _knownName = MutableLiveData<Boolean>()

    val knownName: LiveData<Boolean>
        get() = _knownName

    // Create a Job and a CoroutineScope using the Main Dispatcher
    private var viewModelJob = Job()
    private val coroutineScope = CoroutineScope(viewModelJob + Dispatchers.Main )

    init {
        Log.i("TitleViewModel", "Title View Model created!")
        _failure.value = false
    }

    fun getWeatherConditionByCoordinates(coords: String) {
        coroutineScope.launch {
            var getWeatherDeferred = SurfApi.retrofitService.getWeather("e25099852be74a44add204536210101", "json", "24",
                coords.toString()
            )
            try {
                var listResult = getWeatherDeferred.await().weatherData.weather[0].hourly
                if(listResult.isNullOrEmpty()){
                    _success.value = false
                    _failure.value = true
                    }else if (listResult.isNotEmpty()) {
                        if(knownName.value == false){
                            _spotName.value = "Coords: " + coords
                        }
                        _weatherCondition.value = listResult[0]
                        _failure.value = false
                        _success.value = true
                    }
            } catch (e: Exception) {
                _success.value = false
                _failure.value = true
//                _status.value = "Failure: ${e.message}"
            }
        }
    }

    fun getWeatherByLocation(location: String){
        coroutineScope.launch {
            var getCoordinatesDeferred = SurfApi2.retrofitService2.getCoordinates(location)
            try {
                var listResult = getCoordinatesDeferred.await()
                if(listResult[0].equals(null) || listResult[1].equals(null)){
                    _failure.value = true
                    _success.value = false
                }else{
                    _success.value = false
                    _failure.value = false
                    val coordinates: String = listResult[0] + "," + listResult[1]
                    _spotName.value = location
                    _knownName.value = true
                    getWeatherConditionByCoordinates(coordinates)
                }
            } catch (e: Exception) {
                _success.value = false
                _failure.value = true
                //_status2.value = "Failure: ${e.message}"
            }
        }
    }

    fun getAllSpots() {
        coroutineScope.launch {
            var getSpotsDeferred = SurfApi2.retrofitService2.getAllSpots()
            try {
                var listResult = getSpotsDeferred.await()
                _spots.value = listResult
                _spotsFetched.value = true
            } catch (e: Exception) {
                _failure.value = true
                _spotsFetched.value = false
            }
        }
    }

    public fun setSpotName(spotName: String){
        _spotName.value = spotName
        _knownName.value = true
        _spotsFetched.value = true
    }

    fun onBackToTitleView() {
        _failure.value = false
        _success.value = false
        _spotsFetched.value = false
        _knownName.value = false
    }

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }


}
