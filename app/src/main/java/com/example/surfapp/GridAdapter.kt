package com.example.surfapp

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.surfapp.databinding.GridViewItemBinding
import com.example.surfapp.network.SurfSpot

class GridAdapter(val onClickListener: OnClickListener) : ListAdapter<SurfSpot, GridAdapter.SurfSpotViewHolder>(DiffCallback) {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): GridAdapter.SurfSpotViewHolder {
        return SurfSpotViewHolder(GridViewItemBinding.inflate(LayoutInflater.from(parent.context)))
    }

    override fun onBindViewHolder(holder: GridAdapter.SurfSpotViewHolder, position: Int) {
        val surfSpot = getItem(position)
        holder.bind(surfSpot)
        holder.itemView.setOnClickListener {
            onClickListener.onClick(surfSpot)
        }
    }

    companion object DiffCallback : DiffUtil.ItemCallback<SurfSpot>() {
        override fun areItemsTheSame(oldItem: SurfSpot, newItem: SurfSpot): Boolean {
            return oldItem === newItem
        }

        override fun areContentsTheSame(oldItem: SurfSpot, newItem: SurfSpot): Boolean {
            return oldItem.name == newItem.name
        }
    }

    class SurfSpotViewHolder(private var binding: GridViewItemBinding):
        RecyclerView.ViewHolder(binding.root) {
        fun bind(spot: SurfSpot) {
            binding.spot = spot
            binding.executePendingBindings()
        }
    }

    class OnClickListener(val clickListener: (marsProperty: SurfSpot) -> Unit) {
        fun onClick(surfSpot:SurfSpot) = clickListener(surfSpot)
    }
}