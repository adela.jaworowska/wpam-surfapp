package com.example.surfapp

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.example.surfapp.databinding.FragmentSpotsListBinding

/**
 * A simple [Fragment] subclass.
 * Use the [SpotsListFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class SpotsListFragment : Fragment() {

    private val viewModel: TitleViewModel by activityViewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = DataBindingUtil.inflate<FragmentSpotsListBinding>(inflater, R.layout.fragment_spots_list, container, false)

        // Giving the binding access to the OverviewViewModel
        binding.viewModel = viewModel

        binding.lifecycleOwner = this

        binding.spotsListLayout.setBackgroundResource(R.drawable.top_view_sand_meeting_seawater)

        binding.photosGrid.adapter = GridAdapter(GridAdapter.OnClickListener {
            viewModel.setSpotName(it.name)
            val cords: String = it.lat + "," + it.lon
            viewModel.getWeatherConditionByCoordinates(cords)
        })

        viewModel.success.observe(viewLifecycleOwner, Observer { f ->
            if(f == true){
                findNavController().navigate(R.id.action_spotsListFragment_to_spotDetailsFragment)
                viewModel.onBackToTitleView()
            }
        })

        return binding.root
    }

    companion object {
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            SpotsListFragment().apply {

            }
    }
}