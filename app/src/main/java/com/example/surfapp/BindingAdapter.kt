package com.example.surfapp

import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.surfapp.network.SurfSpot

/**
 * When there is no Surf spots data (data is null), hide the [RecyclerView], otherwise show it.
 */
@BindingAdapter("listData")
fun bindRecyclerView(recyclerView: RecyclerView, data: List<SurfSpot>?) {
    val adapter = recyclerView.adapter as GridAdapter
    adapter.submitList(data)
}