package com.example.surfapp

import android.os.Bundle
import androidx.navigation.fragment.findNavController
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager.widget.ViewPager
import com.example.surfapp.databinding.FragmentDifficultyLevelBinding
import com.google.android.material.tabs.TabLayout
import org.w3c.dom.Text

//import com.example.surfapp.databinding.FragmentDifficultyLevelBinding



/**
 * A simple [Fragment] subclass.
 * Use the [DifficultyLevelFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class DifficultyLevelFragment : Fragment() {

    /**
     * Lazily initialize our [TitleViewModel].
     */
    private val viewModel: TitleViewModel by activityViewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        //return inflater.inflate(R.layout.fragment_difficulty_level, container, false)

        // Inflate the layout for this fragment
        val binding = DataBindingUtil.inflate<FragmentDifficultyLevelBinding>(inflater, R.layout.fragment_difficulty_level , container, false)

        // Giving the binding access to the OverviewViewModel
        binding.viewModel = viewModel

        binding.lifecycleOwner = this

        binding.difficultyLevelLayout.setBackgroundResource(R.drawable.top_view_sand_meeting_seawater)

        if((viewModel.waveHeight)!!.toDouble().compareTo(0.5) <= 0) {
            binding.textView2.setText("NO WAVES")
        }else if((viewModel.waveHeight)!!.toDouble().compareTo(1.0) <= 0){
           binding.textView2.setText("EASY")
           binding.imageView1.setImageResource(android.R.drawable.btn_star_big_on)
        }else if ((viewModel.waveHeight)!!.toDouble().compareTo(1.5) <= 0){
            binding.textView2.setText("MEDIUM")
            binding.imageView1.setImageResource(android.R.drawable.btn_star_big_on)
            binding.imageView2.setImageResource(android.R.drawable.btn_star_big_on)
            binding.imageView3.setImageResource(android.R.drawable.btn_star_big_on)
        }else if ((viewModel.waveHeight)!!.toDouble().compareTo(1.5) > 0){
            binding.textView2.setText("DIFFICULT")
            binding.imageView1.setImageResource(android.R.drawable.btn_star_big_on)
            binding.imageView2.setImageResource(android.R.drawable.btn_star_big_on)
            binding.imageView3.setImageResource(android.R.drawable.btn_star_big_on)
            binding.imageView4.setImageResource(android.R.drawable.btn_star_big_on)
            binding.imageView5.setImageResource(android.R.drawable.btn_star_big_on)
        }

        return binding.root
    }

}