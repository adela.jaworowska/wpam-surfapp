package com.example.surfapp.network

import com.squareup.moshi.Json


data class WeatherCondition(
    @Json(name = "data.weather[0].hourly[0].sigHeight_m")
    val waveHeight: String,

    @Json(name = "data.weather[0].hourly[0].waterTemp_C")
    val waterTemperature: String,

    @Json(name = "data.weather[0].hourly[0].tempC")
    val airTemperature: String

)

data class AllData(

    @Json(name = "data")
    val weatherData: WeatherData
)

data class WeatherData(

    @Json(name = "weather")
    val weather: List<Weather>
)
data class Weather(

    @Json(name = "hourly")
    val hourly: List<Hourly>
)

data class Hourly(
    @Json(name = "sigHeight_m")
    val waveHeight: String,

    @Json(name = "swellHeight_m")
    val swellHeight: String,

    @Json(name = "swellPeriod_secs")
    val swellPeriod: String,

    @Json(name = "waterTemp_C")
    val waterTemperature: String,

    @Json(name = "tempC")
    val airTemperature: String,

    @Json(name = "FeelsLikeC")
    val feelsLike: String,

    @Json(name = "cloudcover")
    val cloudCover: String

)

data class Description(
    @Json(name = "value")
    val descriptionValue: String
)

data class WeatherIcon(
        @Json(name = "value")
        val weatherIconUrl: String
)
