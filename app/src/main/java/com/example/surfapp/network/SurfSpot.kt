package com.example.surfapp.network;

data class SurfSpot(

 val area: String,
 val country: String,
 val lat: String,
 val lon: String,
 val name: String
)

