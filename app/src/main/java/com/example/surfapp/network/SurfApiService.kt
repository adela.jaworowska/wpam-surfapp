package com.example.surfapp.network

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import kotlinx.coroutines.Deferred
import org.json.JSONArray
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

private const val API_KEY = "e25099852be74a44add204536210101"
private const val RESPONSE_FORMAT = "json"

private const val BASE_URL = "https://api.worldweatheronline.com/premium/v1/"
private const val BASE_URL2 = "https://surfspotsapi.com/"

// For json parsing
private val moshi = Moshi.Builder()
    .add(KotlinJsonAdapterFactory())
    .build()

private val retrofit = Retrofit.Builder()
    //.addConverterFactory(ScalarsConverterFactory.create())
    .addConverterFactory(MoshiConverterFactory.create(moshi)) // added for parsing json
    .addCallAdapterFactory(CoroutineCallAdapterFactory())
    .baseUrl(BASE_URL)
    .build()

private val retrofit2 = Retrofit.Builder()
    //.addConverterFactory(ScalarsConverterFactory.create())
    .addConverterFactory(MoshiConverterFactory.create(moshi)) // added for parsing json
    .addCallAdapterFactory(CoroutineCallAdapterFactory())
    .baseUrl(BASE_URL2)
    .build()

interface SurfApiService {

//    /** Coordinates of  the place can be checked here: https://www.latlong.net/ */

    /** Coordinates of  the place can be checked here: https://www.latlong.net/ */
    @GET("marine.ashx")
    fun getWeather(@Query("key") key: String,
                   @Query("format") format: String,
                   @Query("tp") tp: String,
                   @Query("q") geo: String):
            Deferred<AllData>

}

interface SurfApiService2 {

    @GET("api/coords")
    fun getCoordinates(@Query("location") location: String):
            Deferred<List<String>>

    @GET("api/spots")
    fun getAllSpots():
            Deferred<List<SurfSpot>>
}

object SurfApi {
    val retrofitService : SurfApiService by lazy {
        retrofit.create(SurfApiService::class.java)
    }
}

object SurfApi2 {
    val retrofitService2: SurfApiService2 by lazy {
        retrofit2.create(SurfApiService2::class.java)
    }
}