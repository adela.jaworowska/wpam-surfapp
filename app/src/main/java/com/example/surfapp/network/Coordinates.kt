package com.example.surfapp.network

import com.squareup.moshi.Json

data class Coordinates(
    @Json(name="$")
    val coordinates: List<String>,
)