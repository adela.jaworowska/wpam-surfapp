package com.example.surfapp

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.NavigationUI
import com.example.surfapp.databinding.FragmentTitleBinding

/**
 * A simple [Fragment] subclass.
 * Use the [TitleFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class TitleFragment : Fragment() {

    /**
     * Lazily initialize our [TitleViewModel].
     */
//    private val viewModel: TitleViewModel by lazy {
//        ViewModelProvider(this).get(TitleViewModel::class.java)
//    }
    private val viewModel: TitleViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val binding = DataBindingUtil.inflate<FragmentTitleBinding>(inflater, R.layout.fragment_title, container, false)

        // Giving the binding access to the OverviewViewModel
        binding.viewModel = viewModel

        binding.lifecycleOwner = this

        binding.searchNameButton.setOnClickListener{

            binding.spotInput.hideKeyboard()

            val location: String = binding.spotInput.text.toString()
            viewModel.getWeatherByLocation(location)
        }

        binding.searchCoordsButton.setOnClickListener {

            binding.latInput.hideKeyboard()

            val coordinates: String = binding.latInput.text.toString() + "," + binding.lngInput.text.toString()
            viewModel.getWeatherConditionByCoordinates(coordinates)
        }

        binding.showSpotsButton.setOnClickListener{
            viewModel.getAllSpots()
        }

        viewModel.failure.observe(viewLifecycleOwner, Observer { f ->
            if(f == true){
                Toast.makeText(context, "No such spot man :(", Toast.LENGTH_LONG).show()
                binding.latInput.setText("")
                binding.lngInput.setText("")
                binding.spotInput.setText("")
            }
        })

        viewModel.success.observe(viewLifecycleOwner, Observer { f ->
            if(f == true){
                findNavController().navigate(R.id.action_titleFragment_to_spotDetailsFragment)

                //val intent = Intent(activity, MainActivity2::class.java);
                //startActivity(intent)

                viewModel.onBackToTitleView()
                binding.latInput.setText("")
                binding.lngInput.setText("")
                binding.spotInput.setText("")
            }
        })

        viewModel.spotsFetched.observe(viewLifecycleOwner, Observer { f ->
            if(f == true){
                findNavController().navigate(R.id.action_titleFragment_to_spotsListFragment)
                viewModel.onBackToTitleView()
                binding.latInput.setText("")
                binding.lngInput.setText("")
                binding.spotInput.setText("")
            }
        })

        //enable overflow menu in the title fragment
        setHasOptionsMenu(true)
        return binding.root
    }

    fun View.hideKeyboard() {
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(windowToken, 0)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.overflow_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return NavigationUI.onNavDestinationSelected(item!!, view!!.findNavController())
                ||  super.onOptionsItemSelected(item)
    }

    override fun onResume() {
        super.onResume()
        viewModel.onBackToTitleView()
    }
}